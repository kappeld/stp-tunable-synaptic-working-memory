# Working memory model as proposed by Mongillio et al. 2008.
#
# David Kappel
# 16.10.2023
#
# see: https://science.sciencemag.org/content/319/5869/1543.short
#

import numpy as np
import pylab as pl
import nest

from utils import get_snr
from parameters import *
 

def mongillo_etal_model(logn_mu, logn_sigma, p_facilitate=0.05, facilitation = 10., plot=True, seed=None):

    # initialize NEST

    if not 'stochastic_lognormal_synapse' in nest.Models():
        nest.Install("lnsynmodule")

    nest.ResetKernel()
    nest.SetKernelStatus({"local_num_threads":3})

    N_vp = nest.GetKernelStatus(['total_num_virtual_procs'])[0]
    
    if not seed:
        seed = np.random.randint(100000)
    
    nest.SetKernelStatus({"grng_seed" : seed+N_vp})
    nest.SetKernelStatus({"rng_seeds" : range(seed+N_vp+1, seed+2*N_vp+1)})

    # network parameters

    N_E = 5000 # number of excitatory neurons
    N_I = 1000 # number of inhibitory neurons

    # current-based LIF neuron

    theta = 20.      # [mV] neural firing threshold
    tau_m_exc = 15.  # [ms] membrane time constant
    tau_m_inh = 10.  # [ms] membrane time constant
    tau_arp = 2.     # [ms] refractory time
    V_r_exc = 16.    # [mV] reset potential
    V_r_inh = 13.    # [mV] reset potential

    # Gaussian external current

    mu_ext_exc = 0.025*23.10   # [mV] mean external current (excitatory)
    mu_ext_inh = 0.025*21.10   # [mV] mean external current (inhibitory)
    sigma_ext = 1.0      # [mV] sigma of external current

    p = 5     # number of items to be memorized
    f = 0.10  # coding level
    c = 0.20  # connectivity level

    N_pop = int(f*N_E)   # number of neurons per population

    N_syn = int(c*(N_E+N_I)) # number of presynaptic synapses

    N_in = 1000

    N_con_pop = int(c*f*N_E) # number of randomly selected connections from each of the selective populations

    N_con_non = int(c*(1-f*p)*N_E) # number of randomly selected connections from the non-selective excitatory population
    
    N_con_inh = int(c*N_I) # randomly selected connections from the inhibitory population

    # Synapses connecting a selective neuron to a neuron from another selective population or to a non-selective neuron, have baseline efficacy

    # The remaining synapses (i.e. non-selective to selective and non-selective to non-selective) have potentiated efficacy with probability 0.1.

    J_IE = 0.100 # [mV] synaptic efficacy E->I
    J_EI = 0.200 # [mV] synaptic efficacy I->E
    J_II = 0.200 # [mV] synaptic efficacy I->I
    J_b = 0.100  # [mV] baseline synaptic efficacy
    J_p = 0.600  # [mV] potentiated synaptic efficacy

    gamma_0 = 0.10 # fraction of potentiated synapses before learning

    delta = 1.0 # [ms] synaptic delay


    # short-term synaptic dynamics parameters

    U = 0.20        # Baseline utilization factor
    tau_F = 15000.  # [ms] recovery time of utilization factor
    tau_D = 20.    # [ms] recovery time of synaptic resources

    T_recall = 500.0
    T_store = 700.0
    T_delay = 1000.0
    T_init = 300.0


    #U = 0.20        # Baseline utilization factor
    #tau_F = 15000.  # [ms] recovery time of utilization factor
    #tau_D = 200.    # [ms] recovery time of synaptic resources


    # set LIF neuron parameters

    nest.CopyModel('iaf_psc_delta', 'iaf_exc')

    nest.SetDefaults('iaf_exc',
           { 'E_L' : 0., # Resting membrane potential in mV.  
             'C_m' : 1., # apacity of the membrane in pF  
             'tau_m' : tau_m_exc, # Membrane time constant in ms.
             't_ref' : tau_arp, # Duration of refractory period (V_m = V_reset) in ms.
             'V_th' : theta, # Spike threshold in mV.  
             'V_reset' : V_r_exc, # Reset membrane potential after a spike in mV.  
             'I_e' : 0. } ) # Constant input current in pA.


    nest.CopyModel('iaf_psc_delta', 'iaf_inh')

    nest.SetDefaults('iaf_inh',
           { 'E_L' : 0., # Resting membrane potential in mV.  
             'C_m' : 1., # apacity of the membrane in pF  
             'tau_m' : tau_m_inh, # Membrane time constant in ms.
             't_ref' : tau_arp, # Duration of refractory period (V_m = V_reset) in ms.
             'V_th' : theta, # Spike threshold in mV.  
             'V_reset' : V_r_inh, # Reset membrane potential after a spike in mV.  
             'I_e' : 0. } ) # Constant input current in pA.

    stp_synapse_type = 'stochastic_lognormal_synapse'

    nest.CopyModel(stp_synapse_type, 'exc_syn_pot')

    data_mean = np.exp(logn_mu + (logn_sigma**2)/2)

    print("mean retention time is: "+str(data_mean)+" ms")

    nest.SetDefaults('exc_syn_pot',
                     {"facilitation":facilitation, "p_facilitate":p_facilitate,
                      "mu":logn_mu, "sigma":logn_sigma, "weight": J_p/facilitation})

    nest.CopyModel(stp_synapse_type, 'exc_syn_bas')

    nest.SetDefaults('exc_syn_bas',
                     {"facilitation":facilitation, "p_facilitate":p_facilitate,
                      "mu":logn_mu, "sigma":logn_sigma, "weight": J_b/facilitation})

    nest.CopyModel('static_synapse', 'inh_exc_syn')

    nest.SetDefaults('inh_exc_syn', {'weight': -J_IE})

    nest.CopyModel('static_synapse', 'exc_inh_syn')

    nest.SetDefaults('exc_inh_syn', {'weight': J_EI})

    nest.CopyModel('static_synapse', 'inh_inh_syn')

    nest.SetDefaults('inh_inh_syn', {'weight': -J_II})

    nest.CopyModel('static_synapse', 'input_synapse')

    nest.SetDefaults('input_synapse', {'weight': 0.5})


    # set up network

    print('setting up network ...')


    exc_input = [ nest.Create('iaf_exc', n) for n in [N_in]*p ]

    exc_specific = [ nest.Create('iaf_exc', n) for n in [N_pop]*p ]

    exc_non_spec = nest.Create('iaf_exc', N_E - p*N_pop )

    inh = nest.Create('iaf_inh', N_I)

    ng_exc = nest.Create('noise_generator', params={ 'mean': mu_ext_exc, 'std' : sigma_ext })

    ng_inh = nest.Create('noise_generator', params={ 'mean': mu_ext_inh, 'std' : sigma_ext })

    ng_load_bg_param = {"mean":0.5, "std":1.3}
    ng_store_param = {"mean":0.2, "std":2.5}
    ng_recall_param = {"mean":0.2, "std":2.2}


    ng_load = [ nest.Create('noise_generator', params=ng_load_bg_param) for n in range(p) ]

    sd = nest.Create('spike_detector')


    nest.Connect( ng_exc, exc_non_spec )
    nest.Connect( ng_inh, inh )


    # connect neurons

    print('generating connections ...')

    subs = 10

    for i in range(p):
        pop = exc_specific[i]
        other = sum(exc_specific[:i] + exc_specific[i+1:],())
        nest.Connect( ng_exc, pop )
        nest.Connect( ng_load[i], exc_input[i] )
        nest.Connect( pop, pop, {'rule': 'fixed_indegree', 
                                 'indegree': N_con_pop, 'autapses': False,
                                 'multapses': False}, 'exc_syn_pot')
        nest.Connect( pop, other, {'rule': 'fixed_indegree', 
                                   'indegree': N_con_pop, 'autapses': False,
                                   'multapses': False}, 'exc_syn_bas')
        nest.Connect( pop, exc_non_spec, {'rule': 'fixed_indegree', 
                                 'indegree': N_con_pop, 'autapses': False,
                                 'multapses': False}, 'exc_syn_bas')
        nest.Connect( exc_non_spec, pop, {'rule': 'fixed_indegree', 
                                 'indegree': N_con_non, 'autapses': False,
                                 'multapses': False}, 'exc_syn_bas')
        nest.Connect( pop, inh, {'rule': 'fixed_indegree', 
                                 'indegree': N_con_pop, 'autapses': False,
                                 'multapses': False}, 'exc_inh_syn')
        nest.Connect( inh, pop, {'rule': 'fixed_indegree', 
                                 'indegree': N_con_inh, 'autapses': False,
                                 'multapses': False}, 'inh_exc_syn')
        nest.Connect( exc_input[i], pop, 'all_to_all', 'input_synapse')
        nest.Connect( pop[::subs], sd )
        nest.Connect( exc_input[i][::subs], sd )


    nest.Connect( exc_non_spec, exc_non_spec, {'rule': 'fixed_indegree', 
                  'indegree': N_con_non, 'autapses': False,
                  'multapses': False}, 'exc_syn_bas')

    nest.Connect( exc_non_spec, inh, {'rule': 'fixed_indegree', 
                  'indegree': N_con_non, 'autapses': False,
                  'multapses': False}, 'exc_inh_syn')

    nest.Connect( inh, exc_non_spec, {'rule': 'fixed_indegree', 
                             'indegree': N_con_inh, 'autapses': False,
                             'multapses': False}, 'inh_exc_syn')

    nest.Connect( inh, inh, {'rule': 'fixed_indegree', 
                             'indegree': N_con_inh, 'autapses': False,
                             'multapses': False}, 'inh_inh_syn')


    nest.Connect( exc_non_spec[::subs], sd )
    nest.Connect( inh[::subs], sd )

    # simulate network

    nest.Simulate( T_init )

    # loading phase 1

    t_loading_phase_1 = (T_init, T_init+T_store)

    for i in range(p):
        nest.SetStatus( ng_load[i], ng_store_param if i==2 else ng_load_bg_param )

    nest.Simulate( T_store )

    for i in range(p):
        nest.SetStatus( ng_load[i], ng_load_bg_param )

    nest.Simulate( T_delay )

    # recall phase 1

    t_recall_phase_1 = (t_loading_phase_1[1]+T_delay,t_loading_phase_1[1]+T_delay+T_recall)

    for i in range(p):
        nest.SetStatus( ng_load[i], ng_recall_param )

    nest.Simulate( T_recall )

    for i in range(p):
        nest.SetStatus( ng_load[i], ng_load_bg_param )

    nest.Simulate( T_delay )

    # recall phase 2

    t_recall_phase_2 = (t_recall_phase_1[1]+T_delay, t_recall_phase_1[1]+T_delay+T_recall)

    for i in range(p):
        nest.SetStatus( ng_load[i], ng_recall_param )

    nest.Simulate( T_recall )

    for i in range(p):
        nest.SetStatus( ng_load[i], ng_load_bg_param )

    nest.Simulate( T_delay )

    # loading phase 2

    t_loading_phase_2 = (t_recall_phase_2[1]+T_delay,t_recall_phase_2[1]+T_delay+T_store)

    for i in range(p):
        nest.SetStatus( ng_load[i], ng_store_param if i==0 else ng_load_bg_param )

    nest.Simulate( T_store )

    for i in range(p):
        nest.SetStatus( ng_load[i], ng_load_bg_param )

    nest.Simulate( T_recall )

    # recall phase 3

    t_recall_phase_3 = (t_loading_phase_2[1]+T_delay,t_loading_phase_2[1]+T_delay+T_recall)

    for i in range(p):
        nest.SetStatus( ng_load[i], ng_recall_param )

    nest.Simulate( T_store )

    for i in range(p):
        nest.SetStatus( ng_load[i], ng_load_bg_param )

    nest.Simulate( T_store )

    events = nest.GetStatus(sd, 'events')[0]

    def filter_rates(min_max_times):
        """
        auxiliary function to filter time frame from spike train.
        """
        (t_min, t_max) = min_max_times
        rates = []
        senders = []
        group_boundaries = [None]*p
        for n in range(p):
            idx = [ np.where( events['senders'] == exc_specific[n][i] )[0] for i in range(0,N_pop,subs) ]
            rates += [ np.logical_and(events['times'][i] >= t_min, events['times'][i] < t_max).sum() for i in idx ]
            group_boundaries[n] = (n*(N_pop//subs), (n+1)*(N_pop//subs))
            senders += [ exc_specific[n][i] for i in range(0,N_pop,subs) ]
        return rates,senders,group_boundaries

    t_phases = [t_recall_phase_1, t_recall_phase_2, t_recall_phase_3]
    pat_phases = [2,2,0]
    rsgs = [filter_rates(t_phase) for t_phase in t_phases]
    snrs = [get_snr(rsg[0], rsg[2][pat]) for rsg,pat in zip(rsgs,pat_phases)]

    print("mean SNR: ",np.mean(snrs))

    if plot:
        pl.plot( events['times'], events['senders'], 'k|', markersize=0.5)

        for i in range(len(t_phases)):
            pl.plot( np.array(rsgs[i][0]) + t_phases[i][1] + 100, rsgs[i][1] )
            pl.plot( [t_phases[i][1] + 100]*2, [0,10000], 'b-' )
            pl.plot( [t_phases[i][1],t_phases[i][1]+100], [0,0], 'r-' )

        pl.show()

    return snrs


if __name__ == '__main__':
    mongillo_etal_model(logn_mu, logn_sigma)

