/*
 *  This file is part of LNSyn.
 *
 *  Copyright (C) 2019 the LNSyn Authors.
 *
 *  LNSyn is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  LNSyn is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LNSyn.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For more information see: https://gitlab.gwdg.de/kappel2/memristor_working_memory
 * 
 * File:   lognormal_synapse_names.h
 * Author: Kappel
 *
 * Created on October 22, 2019
 */

#ifndef LNSYN_NAMES_H
#define LNSYN_NAMES_H

#include "name.h"

namespace lnsyn
{
/**
 * This namespace contains global Name objects. These can be used in
 * Node::get_status and Node::set_status to make data exchange
 * more efficient and consistent. Creating a Name from a std::string
 * is in O(log n), for n the number of Names already created. Using
 * predefined names should make data exchange much more efficient.
 */
namespace names
{
extern const Name weight_factor;
extern const Name facilitation_time;
extern const Name facilitation;
extern const Name p_facilitate;
}

}

#endif
