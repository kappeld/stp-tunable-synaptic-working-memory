/*
 *  This file is part of LNSyn.
 *
 *  Copyright (C) 2019 the LNSyn Authors.
 *
 *  LNSyn is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  LNSyn is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LNSyn.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For more information see: https://gitlab.gwdg.de/kappel2/memristor_working_memory
 * 
 * File:   lognormal_synapse_names.cpp
 * Author: Kappel
 *
 * Created on October 22, 2019
 */

#include "lognormal_synapse_names.h"

namespace lnsyn
{

namespace names
{
const Name weight_factor("weight_factor");
const Name facilitation_time("facilitation_time");
const Name facilitation("facilitation");
const Name p_facilitate("p_facilitate");
}

}

