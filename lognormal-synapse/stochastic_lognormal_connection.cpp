/*
 *  This file is part of LNSyn.
 *
 *  Copyright (C) 2019 the LNSyn Authors.
 *
 *  LNSyn is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  LNSyn is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LNSyn.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For more information see: https://gitlab.gwdg.de/kappel2/memristor_working_memory
 *
 * File:   stochastic_lognormal_connection.cpp
 * Author: Kappel
 *
 * This file is based on stdp_dopa_connection.h which is part of NEST
 * (Copyright (C) 2004 The NEST Initiative).
 * See: http://nest-initiative.org/
 */

#include "stochastic_lognormal_connection.h"


namespace lnsyn
{


/**
 * Default constructor.
 */
LNSynCommonProperties::LNSynCommonProperties()
: nest::CommonSynapseProperties(),
  facilitation_(2.0),
  p_facilitate_(0.2)
{
}

/**
 * Destructor.
 */
LNSynCommonProperties::~LNSynCommonProperties()
{
}

/**
 * Status getter function.
 */
void LNSynCommonProperties::get_status(DictionaryDatum& d) const
{
    def<double>(d, names::facilitation, facilitation_);
    def<double>(d, names::p_facilitate, p_facilitate_);
    lognormal_dev_.get_status(d);
    nest::CommonSynapseProperties::get_status(d);
}

/**
 * Status setter function.
 */
void LNSynCommonProperties::set_status(const DictionaryDatum& d, nest::ConnectorModel& cm)
{
  updateValue< double >(d, names::facilitation, facilitation_);
  updateValue< double >(d, names::p_facilitate, p_facilitate_);
  lognormal_dev_.set_status(d);
  nest::CommonSynapseProperties::set_status(d, cm);
}

/**
 * Calibrate all time objects, which are contained in this object.
 * This function is called when the time resolution changes and on
 * simulation startup.
 *
 * @param tc time converter object.
 */
void LNSynCommonProperties::calibrate(const nest::TimeConverter& tc)
{
}

}

