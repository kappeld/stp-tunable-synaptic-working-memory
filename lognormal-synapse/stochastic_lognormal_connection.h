/*
 *  This file is part of LNSyn.
 *
 *  Copyright (C) 2019 the LNSyn Authors.
 *
 *  LNSyn is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  LNSyn is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LNSyn.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  For more information see: https://gitlab.gwdg.de/kappel2/memristor_working_memory
 */

#ifndef STOCHASTIC_LOGNORMAL_CONNECTION_H
#define STOCHASTIC_LOGNORMAL_CONNECTION_H

// Includes from nestkernel:
#include "connection.h"
#include "lognormal_randomdev.h"

#include "lognormal_synapse_names.h"

/* BeginDocumentation
  Name: drop_odd_spike - Synapse dropping spikes with odd time stamps.

  Description:
  This synapse will not deliver any spikes with odd time stamps, while spikes
  with even time stamps go through unchanged.

  Transmits: SpikeEvent

  Remarks:
  This synapse type is provided only for illustration purposes in MyModule.

  SeeAlso: synapsedict
*/

namespace lnsyn
{
/**
 * @brief Class holding the common properties for all synapses of type SynapticSamplingRewardGradientConnection.
 *
 * The parameters, their constraints and their default values are described
 * in detail in the documentation of SynapticSamplingRewardGradientConnection.
 */
class LNSynCommonProperties : public nest::CommonSynapseProperties
{
public:

    LNSynCommonProperties();
    ~LNSynCommonProperties();

    using CommonSynapseProperties::get_status;
    using CommonSynapseProperties::set_status;
    using CommonSynapseProperties::calibrate;

    void get_status(DictionaryDatum& d) const;
    void set_status(const DictionaryDatum& d, nest::ConnectorModel& cm);
    void calibrate(const nest::TimeConverter& tc);

    /**
     * Check spike event.
     */
    void check_event(nest::SpikeEvent&)
    {
    }

    /**
     * Convenience function to random number.
     */
    double drand(nest::thread thread) const
    {
        return nest::kernel().rng_manager.get_rng(thread)->drand();
    }

    // parameters
    double facilitation_;
    double p_facilitate_;

    librandom::LognormalRandomDev lognormal_dev_;
};


/**
 * Connection class for illustration purposes.
 *
 * For a discussion of how synapses are created and represented in NEST 2.6,
 * please see Kunkel et al, Front Neuroinform 8:78 (2014), Sec 3.3.
 */
template < typename targetidentifierT >
class LNSynConnection : public nest::Connection< targetidentifierT >
{
private:
  double weight_; //!< Synaptic weight
  double weight_factor_;
  double facilitation_time_;

public:
  //! Type to use for representing common synapse properties
  typedef LNSynCommonProperties CommonPropertiesType;

  //! Shortcut for base class
  typedef nest::Connection< targetidentifierT > ConnectionBase;

  /**
   * Default Constructor.
   * Sets default values for all parameters. Needed by GenericConnectorModel.
   */
  LNSynConnection()
    : ConnectionBase()
    , weight_( 1.0 )
    , weight_factor_( 1.0 )
    , facilitation_time_( 0.0 )
  {
  }

  //! Default Destructor.
  ~LNSynConnection()
  {
  }

  /**
   * Helper class defining which types of events can be transmitted.
   *
   * These methods are only used to test whether a certain type of connection
   * can be created.
   *
   * `handles_test_event()` should be added for all event types that the
   * synapse can transmit. The methods shall return `invalid_port_`; the
   * return value will be ignored.
   *
   * Since this is a synapse model dropping spikes, it is only for spikes,
   * therefore we only implement `handles_test_event()` only for spike
   * events.
   *
   * See Kunkel et al (2014), Sec 3.3.1, for background information.
   */
  class ConnTestDummyNode : public nest::ConnTestDummyNodeBase
  {
  public:
    using nest::ConnTestDummyNodeBase::handles_test_event;
    nest::port
    handles_test_event( nest::SpikeEvent&, nest::rport )
    {
      return nest::invalid_port_;
    }

    nest::port
    handles_test_event( nest::DSSpikeEvent&, nest::rport )
    {
      return nest::invalid_port_;
    }
  };

  /**
   * Check that requested connection can be created.
   *
   * This function is a boilerplate function that should be included unchanged
   * in all synapse models. It is called before a connection is added to check
   * that the connection is legal. It is a wrapper that allows us to call
   * the "real" `check_connection_()` method with the `ConnTestDummyNode
   * dummy_target;` class for this connection type. This avoids a virtual
   * function call for better performance.
   *
   * @param s  Source node for connection
   * @param t  Target node for connection
   * @param receptor_type  Receptor type for connection
   * @param lastspike Time of most recent spike of presynaptic (sender) neuron,
   *                  not used here
   */
  void
  check_connection( nest::Node& s,
    nest::Node& t,
    nest::rport receptor_type,
    double,
    const CommonPropertiesType& )
  {
    ConnTestDummyNode dummy_target;
    ConnectionBase::check_connection_( dummy_target, s, t, receptor_type );
  }

  /**
   * Send an event to the receiver of this connection.
   * @param e The event to send
   * @param t Thread
   * @param t_lastspike Point in time of last spike sent.
   * @param cp Common properties to all synapses.
   */
  void send( nest::Event& e,
    nest::thread t,
    double t_lastspike,
    const CommonPropertiesType& cp );

  // The following methods contain mostly fixed code to forward the
  // corresponding tasks to corresponding methods in the base class and the w_
  // data member holding the weight.

  //! Store connection status information in dictionary
  void get_status( DictionaryDatum& d ) const;

  /**
   * Set connection status.
   *
   * @param d Dictionary with new parameter values
   * @param cm ConnectorModel is passed along to validate new delay values
   */
  void set_status( const DictionaryDatum& d, nest::ConnectorModel& cm );

  //! Allows efficient initialization on contstruction
  void
  set_weight( double w )
  {
    weight_ = w;
  }

  double get_weight() const
  {
    return weight_factor_*weight_;
  }
};


template < typename targetidentifierT >
inline void
LNSynConnection< targetidentifierT >::send( nest::Event& e,
  nest::thread t,
  double last,
  const CommonPropertiesType& props )
{
  const double current_time = e.get_stamp().get_ms();

  const bool is_facilitated = current_time < facilitation_time_;
  
  if (is_facilitated or (nest::kernel().rng_manager.get_rng(t)->drand() < props.p_facilitate_))
  {
    facilitation_time_ = current_time + props.lognormal_dev_(nest::kernel().rng_manager.get_rng(t));
  }

  if (is_facilitated)
  {
    weight_factor_ = props.facilitation_;
  }
  else
  {
    weight_factor_ = 1.0;
  }

  // Even time stamp, we send the spike using the normal sending mechanism
  // send the spike to the target
  e.set_weight( get_weight() );
  e.set_delay( ConnectionBase::get_delay_steps() );
  e.set_receiver( *ConnectionBase::get_target( t ) );
  e.set_rport( ConnectionBase::get_rport() );
  e(); // this sends the event
}

template < typename targetidentifierT >
void
LNSynConnection< targetidentifierT >::get_status(
  DictionaryDatum& d ) const
{
  ConnectionBase::get_status( d );
  def< double >( d, nest::names::weight, weight_ );
  def< double >( d, names::weight_factor, weight_factor_ );
  def< double >( d, names::facilitation_time, facilitation_time_ );
  def< long >( d, nest::names::size_of, sizeof( *this ) );
}

template < typename targetidentifierT >
void
LNSynConnection< targetidentifierT >::set_status(
  const DictionaryDatum& d,
  nest::ConnectorModel& cm )
{
  ConnectionBase::set_status( d, cm );
  updateValue< double >( d, nest::names::weight, weight_ );
  updateValue< double >( d, names::weight_factor, weight_factor_ );
  updateValue< double >( d, names::facilitation_time, facilitation_time_ );
}

} // namespace

#endif // drop_odd_spike_connection.h
