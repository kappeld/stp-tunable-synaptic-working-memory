# Working memory model to store symbolic features.
#
# David Kappel
# 07.01.2022
#

import numpy as np
import pylab as pl
import nest

from parameters import *
from utils import get_snr
 

def working_memory_model(logn_mu, logn_sigma, p_facilitate=0.2, facilitation=20.,
                         plot=True, seed=None, store_patterns=None, recall_patterns=None,
                         T_delay=800.0):

    # prepare experiment
    if not store_patterns:
        store_patterns = [[0,1,0,1,0,1,0,0], [1,0,0,1,0,0,0,1]]
        
    if not recall_patterns:
        recall_patterns = [[0,1,0,0,0,0,0,0], [0,0,0,1,0,0,0,0], [1,0,0,0,0,0,0,0]]

    # initialize NEST

    if not 'stochastic_lognormal_synapse' in nest.Models():
        nest.Install("lnsynmodule")

    nest.ResetKernel()
    nest.SetKernelStatus({"local_num_threads":3})

    N_vp = nest.GetKernelStatus(['total_num_virtual_procs'])[0]
    
    if not seed:
        seed = np.random.randint(100000)
    
    nest.SetKernelStatus({"grng_seed" : seed+N_vp})
    nest.SetKernelStatus({"rng_seeds" : range(seed+N_vp+1, seed+2*N_vp+1)})

    # network parameters

    partition = [3,2,3]     # partition of circuit

    N_in = sum(partition)   # number of input neurons
    N_pop = N_in            # number of excitatory neurons
    N_I = len(partition)    # number of inhibitory neurons
    N_aux = 21              # number of auxiliary neurons
    

    # current-based LIF neuron

    theta = 1400.      # [mV] neural firing threshold
    tau_m_exc = 15.    # [ms] membrane time constant
    tau_m_inh = 10.    # [ms] membrane time constant
    tau_arp = 2.       # [ms] refractory time
    V_r_exc = 0.       # [mV] reset potential
    V_r_inh = 0.       # [mV] reset potential

    # The remaining synapses (i.e. non-selective to selective and non-selective to non-selective) have potentiated efficacy with probability 0.1.

    J_p   =  1000.     # [mV] potentiated synaptic efficacy
    J_in  =  1500.     # [mV] input synapse strength
    J_inh =  1500.     # [mV] inhibitory input from auxiliary neurons

    # short-term synaptic dynamics parameters

    U = 0.20           # Baseline utilization factor
    tau_F = 15000.     # [ms] recovery time of utilization factor
    tau_D = 20.        # [ms] recovery time of synaptic resources
    
    ng_base_sc = 100.  # scaling of random genearator inputs

    T_recall = 200.0
    T_store = 200.0
    T_init = 300.0
    T_delay_inter_pattern = 800.0

    # set LIF neuron parameters

    nest.CopyModel('iaf_psc_delta', 'iaf_exc')

    nest.SetDefaults('iaf_exc',
           { 'E_L' : 0., # Resting membrane potential in mV.  
             'C_m' : 1., # apacity of the membrane in pF  
             'tau_m' : tau_m_exc, # Membrane time constant in ms.
             't_ref' : tau_arp, # Duration of refractory period (V_m = V_reset) in ms.
             'V_th' : theta, # Spike threshold in mV.  
             'V_reset' : V_r_exc, # Reset membrane potential after a spike in mV.  
             'I_e' : 0. } ) # Constant input current in pA.


    nest.CopyModel('iaf_psc_delta', 'iaf_inh')

    nest.SetDefaults('iaf_inh',
           { 'E_L' : 0., # Resting membrane potential in mV.  
             'C_m' : 1., # apacity of the membrane in pF  
             'tau_m' : tau_m_inh, # Membrane time constant in ms.
             't_ref' : tau_arp, # Duration of refractory period (V_m = V_reset) in ms.
             'V_th' : theta, # Spike threshold in mV.  
             'V_reset' : V_r_inh, # Reset membrane potential after a spike in mV.  
             'I_e' : 0. } ) # Constant input current in pA.


    stp_synapse_type = 'stochastic_lognormal_synapse'

    nest.CopyModel(stp_synapse_type, 'exc_syn_pot')

    data_mean = np.exp(logn_mu + (logn_sigma**2)/2)

    print("mean retention time is: "+str(data_mean)+" ms")

    nest.SetDefaults('exc_syn_pot',
                     {"facilitation":facilitation, "p_facilitate":p_facilitate,
                      "mu":logn_mu, "sigma":logn_sigma, "weight": J_p/facilitation})

    nest.CopyModel(stp_synapse_type, 'exc_syn_bas')

    nest.SetDefaults('exc_syn_bas',
                     {"facilitation":facilitation, "p_facilitate":p_facilitate,
                      "mu":logn_mu, "sigma":logn_sigma, "weight": J_b/facilitation})


    nest.CopyModel('static_synapse', 'input_synapse')

    nest.SetDefaults('input_synapse', {'weight': J_in})

    nest.CopyModel('static_synapse', 'aux_inh_synapse')

    nest.SetDefaults('aux_inh_synapse', {'weight': -J_inh})


    # set up network

    print('setting up network ...')

    exc_input = nest.Create('iaf_exc', N_in)

    exc_specific = nest.Create('iaf_exc', N_pop)

    exc_auxiliary_in = nest.Create('iaf_exc', N_aux)

    exc_auxiliary_out = nest.Create('iaf_exc', N_aux)

    inh = nest.Create('parrot_neuron', N_I)
    
    ng_load_bg_param = {"mean":ng_base_sc*0.0, "std":ng_base_sc*0.0}
    ng_store_param = {"mean":ng_base_sc*2.0, "std":ng_base_sc*0.0}
    ng_recall_param = {"mean":ng_base_sc*1.2, "std":ng_base_sc*0.0}

    ng_load = nest.Create('noise_generator', N_in, params=ng_load_bg_param)

    sd = nest.Create('spike_detector')


    # connect neurons

    print('generating connections ...')
    
    nest.Connect(ng_load, exc_input, 'one_to_one')
    
    nest.Connect(exc_input, exc_specific, 'one_to_one', 'input_synapse')
    
    s = [[0,1,2],[3,4],[5,6,7]]
    
    all_conn = [(i,j) for p,q in [(s[u],s[v]) for u in range(len(s)) for v in range(u)] for i in p for j in q]
    
    for n,(i,j) in enumerate(all_conn):
        nest.Connect([exc_specific[i], exc_specific[j]], [exc_auxiliary_in[n]], 'all_to_all', 'input_synapse')
        nest.Connect(list(np.delete(exc_specific,[i,j])), [exc_auxiliary_in[n]], 'all_to_all', 'aux_inh_synapse')
        nest.Connect([exc_auxiliary_out[n]], [exc_specific[i], exc_specific[j]], 'all_to_all', 'input_synapse')
    
    nest.Connect(exc_auxiliary_in, exc_auxiliary_out, 'one_to_one', 'exc_syn_pot')
    
    nest.Connect( exc_input, sd )
    nest.Connect( exc_specific, sd )    
    nest.Connect( inh, sd )
    nest.Connect( exc_auxiliary_in, sd )
    nest.Connect( exc_auxiliary_out, sd )
    


    # simulate network

    nest.Simulate( T_init )

    # loading phase 1
    
    load_pat = store_patterns[0]

    for i in range(N_in):
        nest.SetStatus( [ng_load[i]], ng_store_param if load_pat[i] else ng_load_bg_param )

    nest.Simulate( T_store )

    for i in range(N_in):
        nest.SetStatus( [ng_load[i]], ng_load_bg_param )

    nest.Simulate( T_delay )
    
    t_cur = T_init+T_store+T_delay

    # recall phase 1

    t_recall_phase_1 = (t_cur,t_cur+T_recall)

    load_pat = recall_patterns[0]

    for i in range(N_in):
        nest.SetStatus( [ng_load[i]], ng_store_param if load_pat[i] else ng_load_bg_param )

    nest.Simulate( T_recall )

    for i in range(N_in):
        nest.SetStatus( [ng_load[i]], ng_load_bg_param )

    nest.Simulate( T_delay/2 )
    
    t_cur += T_recall+T_delay/2

    # recall phase 2
    
    t_recall_phase_2 = (t_cur,t_cur+T_recall)    

    load_pat = recall_patterns[1]

    for i in range(N_in):
        nest.SetStatus( [ng_load[i]], ng_store_param if load_pat[i] else ng_load_bg_param )

    nest.Simulate( T_recall )

    for i in range(N_in):
        nest.SetStatus( [ng_load[i]], ng_load_bg_param )

    nest.Simulate( T_delay_inter_pattern )

    t_cur += T_recall+T_delay_inter_pattern

    # loading phase 2

    t_loading_phase_2 = (t_cur,t_cur+T_store)

    load_pat = store_patterns[0]

    for i in range(N_in):
        nest.SetStatus( [ng_load[i]], ng_store_param if load_pat[i] else ng_load_bg_param )

    nest.Simulate( T_store )

    for i in range(N_in):
        nest.SetStatus( [ng_load[i]], ng_load_bg_param )

    nest.Simulate( T_delay )
    
    t_cur += T_store+T_delay

    # recall phase 3

    t_recall_phase_3 = (t_cur,t_cur+T_recall)    

    load_pat = recall_patterns[2]

    for i in range(N_in):
        nest.SetStatus( [ng_load[i]], ng_store_param if load_pat[i] else ng_load_bg_param )

    nest.Simulate( T_recall )

    for i in range(N_in):
        nest.SetStatus( [ng_load[i]], ng_load_bg_param )

    nest.Simulate( T_delay )

    t_cur += T_recall+T_delay

    events = nest.GetStatus(sd, 'events')[0]
    
    def filter_rates(min_max_times):
        """
        auxiliary function to filter time frame from spike train.
        """
        (t_min, t_max) = min_max_times
        rates = []
        senders = []
        group_boundaries = [None]*N_in
        for n in range(N_in):
            idx = [ np.where( events['senders'] == exc_specific[n] )[0] ]
            rates += [ np.logical_and(events['times'][i] >= t_min, events['times'][i] < t_max).sum() for i in idx ]
            group_boundaries[n] = (n, (n+1))
            senders += [ exc_specific[n] ]
        return rates,senders,group_boundaries

    t_phases = [t_recall_phase_1, t_recall_phase_2, t_recall_phase_3]
    pat_phases = [2,2,0]
    rsgs = [filter_rates(t_phase) for t_phase in t_phases]
    snrs = [get_snr(rsg[0], rsg[2][pat]) for rsg,pat in zip(rsgs,pat_phases)]

    print("mean SNR: ",np.mean(snrs))

    if plot:
        pl.plot( events['times'], events['senders'], 'k|', markersize=4.0)

        for i in range(len(t_phases)):
            pl.plot( np.array(rsgs[i][0]) + t_phases[i][1] + 100, rsgs[i][1] )
            pl.plot( [t_phases[i][1] + 100]*2, [0,50], 'b-' )
            pl.plot( [t_phases[i][1],t_phases[i][1]+100], [0,0], 'r-' )

        pl.show()
        
    # compute bit error
    bit_error = 0
        
    for i in range(len(partition)):
        il,ih = np.cumsum(np.hstack([[0],partition]))[[i,i+1]]
        bit_error += np.log2(partition[i])*(np.argmax(store_patterns[0][il:ih]) != np.argmax(np.array(rsgs[0][0])[il:ih]))

    print("bit error: "+str(bit_error))

    return bit_error


if __name__ == '__main__':

    delays = np.arange(100,4000,50)
    N_trials = 10
    
    bit_errors = np.zeros([N_trials, len(delays)])

    for i, T_delay in enumerate(delays):
        for n in range(N_trials):
            bit_errors[n, i] = working_memory_model(logn_mu, logn_sigma, p_facilitate=0.5, plot=False, T_delay=T_delay)
            
    pl.plot(delays, bit_errors.mean(0))
    pl.show()
