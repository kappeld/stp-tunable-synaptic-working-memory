import numpy as np

def get_snr(rates, group_boundaries):
    """
    get signal to noise ratio.
    """
    rates = np.array(rates)
    signal = np.maximum(10e-10, rates[group_boundaries[0]:group_boundaries[1]].sum())
    n_signal = group_boundaries[1]-group_boundaries[0]
    noise = np.maximum(10e-10, rates.sum() - signal)
    n_noise = rates.shape[0] - n_signal
    return (signal/noise)*(n_noise/n_signal)

