# STP tunable synaptic working memory

**Saverio Ricci, David Kappel, Christian Tetzlaff, Daniele Ielmini, Erika Covi**

Different real-world cognitive tasks evolve on different relevant timescales. Processing these tasks requires memory mechanisms able to match their specific time constants. In particular, the working memory utilizes mechanisms that span orders of magnitudes of timescales, from milliseconds to seconds or even minutes. This plentitude of timescales is an essential ingredient of working memory tasks like visual or language processing. This degree of flexibility is challenging in analog computing hardware because it requires the integration of several reconfigurable capacitors of different size. Emerging volatile memristive devices present a compact and appealing solution to reproduce reconfigurable temporal dynamics in a neuromorphic network.
    We present a demonstration of working memory using a silver-based memristive device whose key parameters, retention time and switching probability, can be electrically tuned and adapted to the task at hand. First, we demonstrate the principles of working memory in a small scale hardware to execute an associative memory task. Then, we use the experimental data in two larger scale simulations, the first featuring working memory in a biological environment, the second demonstrating associative symbolic working memory. 

See: [https://arxiv.org/abs/2306.14691](https://arxiv.org/abs/2306.14691)

### Getting started

See `requirements.txt` for dependencies.

Run `mongillo_etal_2008_model.py` and `symbolic_working_memory_model.py` to reproduce results.

To install NEST and synapse model see: [https://www.nest-simulator.org/](https://www.nest-simulator.org/).
